.PHONY: native
.PHONY: byte

byte: sp_dir.exe sp_dir_point.exe sp_dir_point_all.exe sp_dir_stream.exe

native: sp_dir.ml sp_dir_point.ml sp_dir_point_all.ml sp_dir_stream.ml
	ocamlopt str.cmxa sp_dir.ml -o sp_dir.exe
	ocamlopt str.cmxa sp_dir_point.ml -o sp_dir_point.exe
	ocamlopt str.cmxa sp_dir_point_all.ml -o sp_dir_point_all.exe
	ocamlopt str.cmxa sp_dir_stream.ml -o sp_dir_stream.exe

sp_dir.exe: sp_dir.ml
	ocamlc str.cma sp_dir.ml -o sp_dir.exe

sp_dir_point.exe: sp_dir_point.ml
	ocamlc str.cma sp_dir_point.ml -o sp_dir_point.exe

sp_dir_point_all.exe: sp_dir_point_all.ml
	ocamlc str.cma sp_dir_point_all.ml -o sp_dir_point_all.exe

sp_dir_stream.exe: sp_dir_stream.ml
	ocamlc str.cma sp_dir_stream.ml -o sp_dir_stream.exe



clean:
	rm -rf *.exe *.o *.cmx *.cmi *.cmo *-result.txt


test: sp_dir.exe sp_dir_point.exe sp_dir_point_all.exe sp_dir_stream.exe
	./sp_dir.exe test.txt
	./sp_dir_point.exe test.txt 1 2
	./sp_dir_point_all.exe test.txt
	./sp_dir_stream.exe test.txt
	./sp_dir.exe test-large.txt
	./sp_dir_point.exe test-large.txt 3 2
	./sp_dir_point_all.exe test-large.txt
	./sp_dir_stream.exe test-large.txt

graphical: sp_dir.exe sp_dir_point.exe sp_dir_point_all.exe sp_dir_stream.exe
	./sp_dir.exe test.txt -
	./sp_dir_point.exe test.txt 1 2 -
	./sp_dir_point_all.exe test.txt -
	./sp_dir_stream.exe test.txt -
	./sp_dir.exe test-large.txt -
	./sp_dir_point.exe test-large.txt 3 2 -
	./sp_dir_point_all.exe test-large.txt -
	./sp_dir_stream.exe test-large.txt -
