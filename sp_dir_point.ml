open List

type region = { left : int; top : int; right : int; bottom : int }

type square = bool array array

let total_region = { left = -1; top = -1; right = -1; bottom = -1 }




let _ = if Array.length Sys.argv < 4 then
  begin
    print_string "Usage : ";
    print_string Sys.argv.(0);
    print_string " filename x y [-]";
    print_newline ();
    print_string "If you give the fifth argument, this will give pseudo-graphical results";
    print_newline ();
    exit 1
  end
let graphical = Array.length Sys.argv <> 4


let filename = Sys.argv.(1)
let x = int_of_string Sys.argv.(2)
let y = int_of_string Sys.argv.(3)
let output_filename =
  if (Str.string_match (Str.regexp ".*\\.txt")  filename 0)
  then String.sub filename 0 (String.length filename - 4) ^ "-point-result.txt"
  else filename ^ "-point-result.txt"


let (w, h, matrix) =
  let inputfile = open_in filename in
  let first_line = input_line inputfile in
  let first_line_nums = Str.split (Str.regexp "[ \t\r\n]") first_line in
  let w = int_of_string (hd first_line_nums) in
  let h = int_of_string (hd (tl first_line_nums)) in
  let _ =
    if w <= 0 then
      begin
        print_string ("width is not valid value: " ^ string_of_int w ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else if h <= 0 then
      begin
        print_string ("height is not valid value: " ^ string_of_int h ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else if x >= w || x < 0 then
      begin
        print_string ("x is not valid value: " ^ string_of_int x ^ " but should be in the range [0, " ^ string_of_int w ^ ")");
        print_newline ();
        exit 1
      end
    else if y >= h || y < 0 then
      begin
        print_string ("y is not valid value: " ^ string_of_int y ^ " but should be in the range [0, " ^ string_of_int h ^ ")");
        print_newline ();
        exit 1
      end
    else () in
  let mtx = Array.make_matrix h w true in
    begin
      for j = 0 to h-1 do
        for i = 0 to w-1 do
          let curline = mtx.(j) in
          let ch = input_char inputfile in
            if ch = 'W' || ch = 'w' then curline.(i) <- false
        done;
        ignore (input_line inputfile)
      done;
      close_in inputfile;
      if not mtx.(y).(x) then
        begin
          print_string ("The given position (" ^ string_of_int x ^ ", " ^ string_of_int y ^ ") is not black: No solution found");
          print_newline ();
          exit 1
        end;
      (w, h, mtx)
    end


let iter_square cell_action line_action =
  for j = 0 to h-1 do
    for i = 0 to w-1 do
      cell_action i j matrix.(j).(i)
    done;
    line_action j
  done


let in_region region i j =
  region.left <= i && i <= region.right && region.top <= j && j <= region.bottom


let output_region_raw output r =
  let string_of_local n =
    if n < 0 then "inf" else string_of_int n in
    begin
      output_string output "(";
      output_string output (string_of_local r.left);
      output_string output ", ";
      output_string output (string_of_local r.top);
      output_string output ")-(";
      output_string output (string_of_local r.right);
      output_string output ", ";
      output_string output (string_of_local r.bottom);
      output_string output ")\n"
    end


let output_region output r =
  begin
    output_string output "(";
    output_string output (string_of_int r.left);
    output_string output ", ";
    output_string output (string_of_int r.top);
    output_string output ")-(";
    output_string output (string_of_int r.right);
    output_string output ", ";
    output_string output (string_of_int r.bottom);
    output_string output ")\n"
  end


let output_square port region =
  if graphical then
    begin
      iter_square
        (fun i j b ->
           output_string
             port
             (if x = i && y = j then "◎" else if in_region region i j then "●" else if b then "■" else "□"))
        (fun _ -> output_string port "\n");
      output_string port "\n"
    end
  else
    output_region port region





(* (x,y)からみた相対的な位置による矩形をunit内の位置座標による矩形に変換する *)
let global_position region =
  let left = if region.left < 0 then 0 else x - region.left + 1 in
  let top = if region.top < 0 then 0 else y - region.top + 1 in
  let right = if region.right < 0 then w-1 else x + region.right - 1 in
  let bottom = if region.bottom < 0 then h-1 else y + region.bottom - 1 in
  { left = left; top = top; right = right; bottom = bottom }


let over t1 t2 =
  t1 < 0 || (t1 >= t2 && t2 >= 0)

let inclusion reg1 reg2 =
  over reg1.left reg2.left && over reg1.top reg2.top && over reg1.right reg2.right && over reg1.bottom reg2.bottom

let distribute_pos regs reg =
  (  let left   = reg.left   in if left   < 0 then [] else map (fun r -> if over r.left   left   then { r with left   = left   } else r) regs)
  @ (let top    = reg.top    in if top    < 0 then [] else map (fun r -> if over r.top    top    then { r with top    = top    } else r) regs)
  @ (let right  = reg.right  in if right  < 0 then [] else map (fun r -> if over r.right  right  then { r with right  = right  } else r) regs)
  @ (let bottom = reg.bottom in if bottom < 0 then [] else map (fun r -> if over r.bottom bottom then { r with bottom = bottom } else r) regs)


let filtering regs r =
  if exists (fun x -> inclusion x r) regs then regs
  else r :: filter (fun x -> not (inclusion r x)) regs


let reduce =
  fold_left filtering []


let transform =
  fold_left (fun regs reg -> reduce (distribute_pos regs reg)) [total_region]




let make_dir i j =
  let left = ref (-1) in
  let top = ref (-1) in
  let right = ref (-1) in
  let bottom = ref (-1) in
    begin
      if x > i then left := x - i;
      if y > j then top := y - j;
      if x < i then right := i - x;
      if y < j then bottom := j - y;
      if x = i && y = j then begin left := 0; top := 0; right := 0; bottom := 0 end;
      { left = !left; top = !top; right = !right; bottom = !bottom }
    end


let enumerate_white () =
  let whites = ref [] in
    iter_square (fun i j b -> if not b then whites := make_dir i j :: !whites) ignore;
    !whites




let _ =
  begin
    print_string (string_of_int w);
    print_string " ";
    print_string (string_of_int h);
    print_newline ();
    iter_square (fun i j b -> print_string (if b then "B" else "W")) (fun _ -> print_newline ());
    let results = transform (enumerate_white ()) in
      begin
        if graphical then iter (output_region_raw stdout) results;
        let output = open_out output_filename in
          begin
            iter (output_square output) (map global_position results);
            close_out output;
            ()
          end
      end
  end
