open List

type region = { left : int; top : int; right : int; bottom : int }

type square = bool array array



let _ = if Array.length Sys.argv < 2 then
  begin
    print_string "Usage : ";
    print_string Sys.argv.(0);
    print_string " filename [-]";
    print_newline ();
    print_string "If you give the third argument, this will give pseudo-graphical results";
    print_newline ();
    exit 1
  end
let graphical = Array.length Sys.argv <> 2

let filename = Sys.argv.(1)
let output_filename =
  if (Str.string_match (Str.regexp ".*\\.txt")  filename 0)
  then String.sub filename 0 (String.length filename - 4) ^ "-result.txt"
  else filename ^ "-result.txt"


let (w, h, matrix) =
  let inputfile = open_in filename in
  let first_line = input_line inputfile in
  let first_line_nums = Str.split (Str.regexp "[ \t\r\n]") first_line in
  let w = int_of_string (hd first_line_nums) in
  let h = int_of_string (hd (tl first_line_nums)) in
  let _ =
    if w <= 0 then
      begin
        print_string ("width is not valid value: " ^ string_of_int w ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else if h <= 0 then
      begin
        print_string ("height is not valid value: " ^ string_of_int h ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else () in
  let mtx = Array.make_matrix h w true in
    begin
      for j = 0 to h-1 do
        for i = 0 to w-1 do
          let curline = mtx.(j) in
          let ch = input_char inputfile in
            if ch = 'W' || ch = 'w' then curline.(i) <- false
        done;
        ignore (input_line inputfile)
      done;
      close_in inputfile;
      (w, h, mtx)
    end


let iter_square cell_action line_action =
  for j = 0 to h-1 do
    for i = 0 to w-1 do
      cell_action i j matrix.(j).(i)
    done;
    line_action j
  done


let total_region = { left = 0; top = 0; right = w-1; bottom = h-1 }



(* 右下から左上に向かって出現する。以下これを前提としたコードにするので注意。 *)
let enumerate_white () =
  let whites = ref [] in
    iter_square (fun i j b -> if not b then whites := (i, j) :: !whites) ignore;
    if graphical then
      begin
        print_string ("number of whites : " ^ string_of_int (length !whites));
        print_newline ()
      end;
    !whites




let in_region region i j =
  region.left <= i && i <= region.right && region.top <= j && j <= region.bottom


let output_region output r =
  begin
    output_string output "(";
    output_string output (string_of_int r.left);
    output_string output ", ";
    output_string output (string_of_int r.top);
    output_string output ")-(";
    output_string output (string_of_int r.right);
    output_string output ", ";
    output_string output (string_of_int r.bottom);
    output_string output ")\n"
  end


let output_square port region =
  if graphical then
    begin
      iter_square
        (fun i j b ->
           output_string
             port
             (if in_region region i j then "●" else if b then "■" else "□"))
        (fun _ -> output_string port "\n");
      output_string port "\n"
    end
  else
    output_region port region


let inclusion reg1 reg2 =
  reg1.left <= reg2.left && reg1.top <= reg2.top && reg1.right >= reg2.right && reg1.bottom >= reg2.bottom


let filtering regs r =
  if exists (fun x -> inclusion x r) regs then regs
  else r :: filter (fun x -> not (inclusion r x)) regs


let reduce =
  fold_left filtering


let fixed_region reg i j =
  reg.top > j+1
(* もっと細かくreg.top = j+1 && reg.left > iも加えて良いけど *)


let contacted reg i j =
  (reg.left = i+1 || reg.right = i-1) && reg.top <= j && j <= reg.bottom
  || (reg.top = j+1 || reg.bottom = j-1) && reg.left <= i && i <= reg.right



let trial_num = ref 0


let distribute_pos (fixed, regs) (i, j) =
  let distribute (fs, nrs, rs) r =
    if fixed_region r i j then (r :: fs, nrs, rs)
    else if in_region r i j then
      let left = r.left in
      let top = r.top in
      let right = r.right in
      let bottom = r.bottom in
      let results = ref [] in
        begin
          if left   <= i && i < right  then results := { r with left   = i+1 } :: !results;
          if top    <= j && j < bottom then results := { r with top    = j+1 } :: !results;
          if right  >= i && i > left   then results := { r with right  = i-1 } :: !results;
          if bottom >= j && j > top    then results := { r with bottom = j-1 } :: !results;
          (fs, nrs, reduce rs !results)
        end
    else if contacted r i j then (fs, nrs, reduce rs [r])
    else (fs, r :: nrs, rs) in
  let (fs, nrs, rs) = fold_left distribute (fixed, [], []) regs in
    begin
      if graphical then
        begin
          trial_num := !trial_num + 1;
          let lfs = length fs in
          let lnrs = length nrs in
          let lrs = length rs in
            print_string (string_of_int !trial_num ^ "'s number of results : fixed " ^ string_of_int lfs ^ "   rest " ^ string_of_int (lnrs + lrs) ^ "   (related " ^ string_of_int lnrs ^ ", unrelated " ^ string_of_int lrs ^ ")");
            print_newline ();
        end;
(*
      print_string "folder";
      print_newline ();
      output_region stdout reg;
      print_newline ();
      iter (output_region stdout) result;
*)
      (fs, nrs @ rs)
    end


let transform =
  fold_left distribute_pos ([], [total_region])




let _ =
  begin
    print_string (string_of_int w);
    print_string " ";
    print_string (string_of_int h);
    print_newline ();
    iter_square (fun i j b -> print_string (if b then "B" else "W")) (fun _ -> print_newline ());
    let results =
      let (fs, rs) = transform (enumerate_white ()) in
        rs @ fs in
      begin
        if graphical then iter (output_region stdout) results;
        let output = open_out output_filename in
          begin
            iter (output_square output) results;
            close_out output
          end
      end
  end
