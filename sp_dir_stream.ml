open List

type region = { left : int; top : int; right : int; bottom : int }



let _ = if Array.length Sys.argv < 2 then
  begin
    print_string "Usage : ";
    print_string Sys.argv.(0);
    print_string " filename [-]";
    print_newline ();
    print_string "In contrast from other programs, this does not provide pseudo-graphical results, but display intermediate information if you give the third argument";
    print_newline ();
    exit 1
  end
let display = Array.length Sys.argv <> 2


let filename = Sys.argv.(1)
let output_filename =
  if (Str.string_match (Str.regexp ".*\\.txt")  filename 0)
  then String.sub filename 0 (String.length filename - 4) ^ "-stream-result.txt"
  else filename ^ "-stream-result.txt"


(* これはキャッチされる予定はありません（エラーで終了） *)
exception FileFormatException

let (w, h) =
  let inputfile = open_in filename in
  let first_line = input_line inputfile in
  let first_line_nums = Str.split (Str.regexp "[ \t\r\n]") first_line in
  let w = int_of_string (hd first_line_nums) in
  let h = int_of_string (hd (tl first_line_nums)) in
  let _ =
    if w <= 0 then
      begin
        print_string ("width is not valid value: " ^ string_of_int w ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else if h <= 0 then
      begin
        print_string ("height is not valid value: " ^ string_of_int h ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else () in
    print_string (string_of_int w);
    print_string " ";
    print_string (string_of_int h);
    print_newline ();
    let w_num = ref 0 in
      for j = 0 to h-1 do
        for i = 0 to w-1 do
          let ch = input_char inputfile in
            if ch = 'W' || ch = 'w' then
              begin
                print_string "W";
                w_num := !w_num + 1
              end
            else if ch = 'B' || ch = 'b' || ch = 'g' || ch = 'G' then print_string "B"
            else
              begin
                output_string stderr "\nThe file contains something wrong\n";
                close_in inputfile;
                raise FileFormatException
              end
        done;
        ignore (input_line inputfile);
        print_newline ()
      done;
      if display then
        begin
          print_string ("number of whites : " ^ string_of_int !w_num);
          print_newline ();
        end;
      close_in inputfile;
      (w, h)


let total_region = { left = 0; top = 0; right = w-1; bottom = h-1 }


exception FinishUnitException

(* 情報源のstream *)
(* 左上から右下に向かって読み込むことになる *)
let next_whiteregion =
  let inputfile = open_in filename in
  let _ = input_line inputfile in
  let cur_w = ref 0 in
  let cur_h = ref 0 in
  let rec f () =
    if !cur_w = w then
      begin
        cur_w := 0;
        cur_h := !cur_h + 1;
        ignore (input_line inputfile)
      end;
    if !cur_h = h then
      begin
        close_in inputfile;
        raise FinishUnitException
      end;
    let i = !cur_w in
      cur_w := i + 1;
      let ch = input_char inputfile in
        if ch = 'W' || ch = 'w' then (i, !cur_h)
        else f ()
  in f




let in_region region i j =
  region.left <= i && i <= region.right && region.top <= j && j <= region.bottom


let output_region output r =
  begin
    output_string output "(";
    output_string output (string_of_int r.left);
    output_string output ", ";
    output_string output (string_of_int r.top);
    output_string output ")-(";
    output_string output (string_of_int r.right);
    output_string output ", ";
    output_string output (string_of_int r.bottom);
    output_string output ")\n"
  end


let inclusion reg1 reg2 =
  reg1.left <= reg2.left && reg1.top <= reg2.top && reg1.right >= reg2.right && reg1.bottom >= reg2.bottom


let filtering regs r =
  if exists (fun x -> inclusion x r) regs then regs
  else r :: filter (fun x -> not (inclusion r x)) regs


let reduce =
  fold_left filtering


let fixed_region reg i j =
  reg.bottom < j-1
(* もっと細かくreg.bottom = j-1 && reg.right < iも加えて良いけど *)


let contacted reg i j =
  (reg.left = i+1 || reg.right = i-1) && reg.top <= j && j <= reg.bottom
  || (reg.top = j+1 || reg.bottom = j-1) && reg.left <= i && i <= reg.right



let trial_num = ref 0
let output = open_out output_filename


let distribute_pos regs (i, j) =
  let fixed_num = ref 0 in
  let distribute (nrs, rs) r =
    if fixed_region r i j then
      begin
        output_region output r;
        fixed_num := !fixed_num + 1;
        (nrs, rs)
      end
    else if in_region r i j then
      let left = r.left in
      let top = r.top in
      let right = r.right in
      let bottom = r.bottom in
      let results = ref [] in
        begin
          if left   <= i && i < right  then results := { r with left   = i+1 } :: !results;
          if top    <= j && j < bottom then results := { r with top    = j+1 } :: !results;
          if right  >= i && i > left   then results := { r with right  = i-1 } :: !results;
          if bottom >= j && j > top    then results := { r with bottom = j-1 } :: !results;
          (nrs, reduce rs !results)
        end
    else if contacted r i j then (nrs, reduce rs [r])
    else (r :: nrs, rs) in
  let (nrs, rs) = fold_left distribute ([], []) regs in
    begin
      if display then
        begin
          trial_num := !trial_num + 1;
          let lnrs = length nrs in
          let lrs = length rs in
            print_string (string_of_int !trial_num ^ "'s number of results : fixed " ^ string_of_int !fixed_num ^ "   rest " ^ string_of_int (lnrs + lrs) ^ "   (related " ^ string_of_int lnrs ^ ", unrelated " ^ string_of_int lrs ^ ")");
            print_newline ();
            fixed_num := 0
        end;
(*
      print_string "folder";
      print_newline ();
      output_region stdout reg;
      print_newline ();
      iter (output_region stdout) result;
*)
      (nrs @ rs)
    end


exception FinishCalc of region list


(* streamの処理、決定されたら途中で出力している *)
let loop () =
  let rec iter regs =
    let next =
      try
        next_whiteregion ()
      with
        | FinishUnitException -> raise (FinishCalc regs) in
      iter (distribute_pos regs next) in
    iter [total_region]


let _ =
  begin
    try loop () with
      | FinishCalc results ->
          iter (output_region output) results
  end;
  close_out output;
  ()
