open List

type region = { left : int; top : int; right : int; bottom : int }

type square = bool array array

let total_region = { left = -1; top = -1; right = -1; bottom = -1 }




let _ = if Array.length Sys.argv < 2 then
  begin
    print_string "Usage : ";
    print_string Sys.argv.(0);
    print_string " filename [-]";
    print_newline ();
    print_string "If you give the third argument, this will give pseudo-graphical results";
    print_newline ();
    exit 1
  end
let graphical = Array.length Sys.argv <> 2

let filename = Sys.argv.(1)
let output_filename =
  if (Str.string_match (Str.regexp ".*\\.txt")  filename 0)
  then String.sub filename 0 (String.length filename - 4) ^ "-point-all-result.txt"
  else filename ^ "-point-all-result.txt"


let (w, h, matrix) =
  let inputfile = open_in filename in
  let first_line = input_line inputfile in
  let first_line_nums = Str.split (Str.regexp "[ \t\r\n]") first_line in
  let w = int_of_string (hd first_line_nums) in
  let h = int_of_string (hd (tl first_line_nums)) in
  let _ =
    if w <= 0 then
      begin
        print_string ("width is not valid value: " ^ string_of_int w ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else if h <= 0 then
      begin
        print_string ("height is not valid value: " ^ string_of_int h ^ " but shoud be positive");
        print_newline ();
        exit 1
      end
    else () in
  let mtx = Array.make_matrix h w true in
    begin
      for j = 0 to h-1 do
        for i = 0 to w-1 do
          let curline = mtx.(j) in
          let ch = input_char inputfile in
            if ch = 'W' || ch = 'w' then curline.(i) <- false
        done;
        ignore (input_line inputfile)
      done;
      close_in inputfile;
      (w, h, mtx)
    end


let iter_square cell_action line_action =
  for j = 0 to h-1 do
    for i = 0 to w-1 do
      cell_action i j matrix.(j).(i)
    done;
    line_action j
  done


let in_region region i j =
  region.left <= i && i <= region.right && region.top <= j && j <= region.bottom


let output_region_raw output r =
  let string_of_local n =
    if n < 0 then "inf" else string_of_int n in
    begin
      output_string output "(";
      output_string output (string_of_local r.left);
      output_string output ", ";
      output_string output (string_of_local r.top);
      output_string output ")-(";
      output_string output (string_of_local r.right);
      output_string output ", ";
      output_string output (string_of_local r.bottom);
      output_string output ")\n"
    end


let output_region output r =
  begin
    output_string output "(";
    output_string output (string_of_int r.left);
    output_string output ", ";
    output_string output (string_of_int r.top);
    output_string output ")-(";
    output_string output (string_of_int r.right);
    output_string output ", ";
    output_string output (string_of_int r.bottom);
    output_string output ")\n"
  end


let output_square port region =
  if graphical then
    begin
      iter_square
        (fun i j b ->
           output_string
             port
             (if in_region region i j then "●" else if b then "■" else "□"))
        (fun _ -> output_string port "\n");
      output_string port "\n"
    end
  else
    output_region port region



(* (x,y)からみた相対的な位置による矩形をunit内の位置座標による矩形に変換する *)
let global_position x y region =
  let left = if region.left < 0 then 0 else x - region.left + 1 in
  let top = if region.top < 0 then 0 else y - region.top + 1 in
  let right = if region.right < 0 then w-1 else x + region.right - 1 in
  let bottom = if region.bottom < 0 then h-1 else y + region.bottom - 1 in
  { left = left; top = top; right = right; bottom = bottom }


let over t1 t2 =
  t1 < 0 || (t1 >= t2 && t2 >= 0)

let inclusion reg1 reg2 =
  over reg1.left reg2.left && over reg1.top reg2.top && over reg1.right reg2.right && over reg1.bottom reg2.bottom

let distribute_pos regs reg =
 (   let top    = reg.top    in if top    < 0 then [] else map (fun r -> if over r.top    top    then { r with top    = top    } else r) regs)
  @ (let right  = reg.right  in if right  < 0 then [] else map (fun r -> if over r.right  right  then { r with right  = right  } else r) regs)
  @ (let bottom = reg.bottom in if bottom < 0 then [] else map (fun r -> if over r.bottom bottom then { r with bottom = bottom } else r) regs)


let filtering regs r =
  if exists (fun x -> inclusion x r) regs then regs
  else r :: filter (fun x -> not (inclusion r x)) regs


let reduce =
  fold_left filtering []


let transform i =
  fold_left (fun regs reg -> reduce (distribute_pos regs reg)) [if i = 0 then total_region else { total_region with left = 1 } ]




let make_dir x y i j =
  let top = ref (-1) in
  let right = ref (-1) in
  let bottom = ref (-1) in
    begin
      if y > j then top := y - j;
      if x < i then right := i - x;
      if y < j then bottom := j - y;
      if x = i && y = j then begin top := 0; right := 0; bottom := 0 end;
      { left = -1; top = !top; right = !right; bottom = !bottom }
    end



let whites =
  let w = ref [] in
    iter_square (fun i j b -> if not b then w := (i, j) :: !w) ignore; rev !w


let enumerate_white x y =
  fold_left (fun ws (i, j) -> if i < x then ws else make_dir x y i j :: ws) [] whites



let inclusion_global reg1 reg2 =
  reg1.left <= reg2.left && reg1.top <= reg2.top && reg1.right >= reg2.right && reg1.bottom >= reg2.bottom

let filtering_global regs r =
  if exists (fun x -> inclusion_global x r) regs then regs
  else r :: regs



let reduce_global =
  fold_left filtering_global


let enumerate_black () =
  let blacks = ref [] in
    iter_square (fun i j b -> if b && (i = 0 || not matrix.(j).(i-1)) then blacks := (i, j) :: ! blacks) ignore;
    if graphical then
      begin
        print_string ("number of checked blacks : " ^ string_of_int (length !blacks));
        print_newline ()
      end;
    !blacks


let trial_num = ref 0


let black_fold regs (x, y) =
  let xy_result = map (global_position x y) (transform x (enumerate_white x y)) in
  let xy_in = filter (fun r -> in_region r x y) regs in
  let result = fold_left (fun rs r -> if exists (fun x -> inclusion_global x r) xy_in then rs else r :: rs) regs xy_result in
    begin
      trial_num := !trial_num + 1;
      if graphical then
        begin
          print_string (string_of_int !trial_num ^ "'s number of results : " ^ string_of_int (length result));
          print_newline ();
        end;
      result
    end



let _ =
  begin
    print_string (string_of_int w);
    print_string " ";
    print_string (string_of_int h);
    print_newline ();
    iter_square (fun i j b -> print_string (if b then "B" else "W")) (fun _ -> print_newline ());
    let blacks = enumerate_black () in
    let results = fold_left black_fold [] blacks in
      begin
        if graphical then iter (output_region stdout) results;
        let output = open_out output_filename in
          begin
            iter (output_square output) results;
            close_out output;
            ()
          end
      end
  end
