# summary

矩形の重ね合わせ可能な領域を、白領域の方向から導出するプログラム。OCamlで記述。

* sp_dir_point : 指定領域を含む極大矩形を列挙するプログラム。これのみ領域の指定が必要。

* sp_dir_point_all : 黒領域（の一部）に対してsp_dir_pointと同様の処理を行うことで、全体の極大矩形を列挙するプログラム。

* sp_dir : 白領域を元に、それが矩形の端に来た場合を想定し、包含関係でふるいにかけることで全体の極大矩形を列挙するプログラム。

* sp_dir_stream : sp_dirを、ユニットを保存することなく、決定された極大矩形を直後に処理することでメモリ使用量を減らしたプログラム。内部には矩形候補しか保存しない。ユニットがないためgraphical出力がない。

[![wercker status](https://app.wercker.com/status/9f744e045c405c130f3641c11e016775/m "wercker status")](https://app.wercker.com/project/bykey/9f744e045c405c130f3641c11e016775)
